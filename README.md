# Name of library

## Description




## Installation

### from gitlab

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/eunomia_data_quality.git
```

Ou

```
pip install git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/eunomia_data_quality.git
```

### test install

Get install directory
```
pip show 
```

Use pytest on path
```
pytest --doctest-modules /path_wherre_pip_install/site-packages/
```



## Usage


```

```

## License
GNU GPL

