"""
JudgeDredd module: for gauges quality control
"""
__license__ = "GNU GPL"



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse

from eunomia_data_quality.misserror import MissError
from eunomia_data_quality.check_daily_rainfall import DailyRain_Checker
from eunomia_data_quality.network_op import *

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--tests', default=False, action='store_true')
    args = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if args.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)
