"""
Miss and Error flag to treat data
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
from numbers import Number

import pandas


##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

MISSERR_CODES = ("not at all", "possible", "likely", "very likely", "virtually certain")
MISSERR_CODES = pandas.Series(MISSERR_CODES)

MAXERR = max(MISSERR_CODES.index)

##======================================================================================================================##
##                MISSING ERROR CLASS                                                                                   ##
##======================================================================================================================##

class MissError:
    """ Class doc
    >>> a = MissError("possible")
    >>> b = MissError("likely")
    >>> c = MissError("very likely")
    >>> d = MissError(4)
    >>> e = MissError(5)
    >>> e.val
    4
    >>> e = MissError(-1)
    Traceback (most recent call last):
        ...
    ValueError: code must be positive, got -1
    
    >>> print(a)
    MissError 1 (possible)
    >>> print(d)
    MissError 4 (virtually certain)
    >>> print(a + a)
    MissError 2 (likely)
    >>> print(a + b)
    MissError 2 (likely)
    >>> print(a + d)
    MissError 4 (virtually certain)
    >>> print(c)
    MissError 3 (very likely)
    >>> c.to_mask_value(0)
    Traceback (most recent call last):
        ...
    AssertionError: pb limit
    >>> c.to_mask_value(1)
    False
    >>> c.to_mask_value(2)
    False
    >>> c.to_mask_value(3)
    False
    >>> c.to_mask_value(4)
    True
    >>> a == a
    True
    >>> a == b
    False
    """
    NAME = "MissError"
    def __init__(self, code):
        """ Class initialiser """
        if isinstance(code, Number):
            if code < 0:
                raise ValueError(f"code must be positive, got {code}")
            elif code <= MAXERR:
                pass
            else:
                code = MAXERR
             #~ <= MAXERR, "must be between 0 and {1}, got : {0}".format(code, MAXERR)
            self.val = int(round(code))
            self.prob = MISSERR_CODES[self.val]
        elif isinstance(code, str):
            assert code in MISSERR_CODES.values, f"must be a valid code, got : {code}"
            self.prob = code
            self.val = int(MISSERR_CODES[MISSERR_CODES == code].index[0])
        else:
            raise ValueError(f'Dont know what to do with {code}')
        
    def __eq__(self, other):
        """
        Test equality
        """
        assert isinstance(other, MissError), "must be misserror to allow comparison"
        res = self.val == other.val
        return res
        
    def __str__(self):
        """
        When using print
        """
        res = f"{self.NAME} {self.val} ({self.prob})"
        return res
        
    def __add__(self, other):
        """
        Increment the value of 1 if possible
        """
        if isinstance(other, MissError):
            if self.val == other.val:
                if self.val == 0:
                    nwvl = 0
                else:
                    nwvl = self.val + 1
            else:
                nwvl = max([self.val, other.val])
            
        else:
            nwvl = self.val
            
        res = MissError(int(nwvl))
        return res
            #~ "exp same class got {0.__class__} vs {1.__class__}".foramt(self, other)
        
    def to_mask_value(self, limit=1.5):
        """
        Return True if code below limit, 
        """
        assert isinstance(limit, Number) and 1 <= limit <= MAXERR, "pb limit"
        res = self.val < limit
        return res

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--tests', default=False, action='store_true')
    args = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if args.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)
        
        
        
    #+++++++++++++++++++++++++++++++#
    #    TEST                       #
    #+++++++++++++++++++++++++++++++#
    if args.tests:
        
        
        a = MissError("likely")
        b = MissError("likely")
        print(a)
        c = a + b
        print(c)
        print(c.to_mask_value(3))
        
