"""
Class for checking daily rainfall series
 - for now simple check on deviation from seasonal cycle and annual statistics
 - to implement:
   * spatial coherence checking
   * daily check  
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import datetime
from numbers import Number
import numpy
import pandas

from eunomia_data_quality.misserror import MissError, MAXERR

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

class DailyRain_Checker(pandas.DataFrame):
    """ Class doc
    >>> nind = 365*10
    >>> nsta=3
    >>> ds = [datetime.datetime(2014,12,28) + datetime.timedelta(1*i) for i in range(nind)]
    >>> dt = numpy.random.exponential(scale = 15,size = nind*3).reshape(nind,3)
    >>> dtf = pandas.DataFrame(dt.copy(), columns = list("abc"), index = ds)
    >>> dtf.loc["2015","c"]=numpy.nan
    >>> drc = DailyRain_Checker(dtf, columns = list("abc"), index = ds)
    >>> drc.loc["2015"].isnull().sum()
    a      0
    b      0
    c    365
    dtype: int64

    >>> emm = drc.empty_mask()
    >>> mat = drc._annual_rainfall_check()
    >>> ym  = drc.fill_yearmask(mat)
    >>> td  = drc.treated_data()
    Years that do not have a sufficient number of stations (nmin=0.6): Int64Index([2014], dtype='int64')
    nrow with only nan : 4
    >>> sorted(set(td.index.year))
    [2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024]
    >>> mk = drc.mask_annual_values()
    >>> mk
              a      b      c
    2014  False  False  False
    2015   True   True  False
    2016   True   True   True
    2017   True   True   True
    2018   True   True   True
    2019   True   True   True
    2020   True   True   True
    2021   True   True   True
    2022   True   True   True
    2023   True   True   True
    2024   True   True   True

    >>> dt1 = dt.copy()
    >>> dt1[100:300,0] = numpy.nan
    >>> dt1[1000:1400,1] = numpy.nan

    >>> drc = DailyRain_Checker(dt1, columns = list("abc"), index = ds)
    >>> drc.loc["2015"].isnull().sum()
    a    200
    b      0
    c      0
    dtype: int64
    >>> drc.loc["2016"].isnull().sum()
    a    0
    b    0
    c    0
    dtype: int64

    >>> drc.loc["2017"].isnull().sum()
    a      0
    b    100
    c      0
    dtype: int64


    >>> emm = drc.empty_mask()
    >>> mat = drc._annual_rainfall_check()
    >>> ym  = drc.fill_yearmask(mat)
    >>> td  = drc.treated_data()
    Years that do not have a sufficient number of stations (nmin=0.6): Int64Index([2014], dtype='int64')
    nrow with only nan : 4
    >>> sorted(set(td.index.year))
    [2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024]
    >>> mk = drc.mask_annual_values()
    >>> mk
              a      b      c
    2014  False  False  False
    2015  False   True   True
    2016   True   True   True
    2017   True  False   True
    2018   True  False   True
    2019   True   True   True
    2020   True   True   True
    2021   True   True   True
    2022   True   True   True
    2023   True   True   True
    2024   True   True   True
    
    
    
    """
    
    def __init__(self, data, **kwarg):
        """ Class initialiser """
        pandas.DataFrame.__init__(self, data, **kwarg)
        
        #~ self.annual_total_check=self._annual_rainfall_check()

    def empty_mask(self):
        """Create an empty mask"""
        res = self.copy()
        res[:] = numpy.nan
        return res
        
    def fill_yearmask(self, mask):
        """Fill from a year mask"""
        res = self.empty_mask()
        
        for iyear in mask.index:
            #get index in new mask
            indx = res.index.year == iyear
            indx = res.index[indx]
            # get maksk values
            mkvals = mask[mask.index == iyear]
            #replicate values
            nrow = indx.size
            nwvals = [mkvals] * nrow
            nwvals = pandas.concat(nwvals)
            #~ print(nwvals)
            nwvals = pandas.DataFrame(nwvals.values, columns=nwvals.columns, index=indx)
            #~ print(iyear, indx, nwvals)
            #~ print(iyear)
            
            res.loc[indx] = nwvals
            #~ for iind in indx: 
                #~ res.iloc[iind] = maskvals

        return res
            
        #~ a.iloc[i] = b[b.index == 2015].values
        

    def _annual_rainfall_check(self):
        """Check annual totals"""
        #annual rainfall
        ar = self.groupby(self.index.year).sum(min_count=1)
        # ~ ar = self.groupby(self.index.year).sum()
        mar = ar.mean()

        res = ar.copy()
        res[:] = 0
        assert res.shape == ar.shape
        
        for codeerror, factor in zip([1, MAXERR - 1, MAXERR], [2, 5, 10]):
            #~ print(codeerror, factor)
            #~ print(MissError(codeerror))
            errs = (ar < (mar / factor)) | (ar > (mar * factor)) # | numpy.isnan(ar)
            res[errs] = codeerror
            
        res = res.applymap(MissError)
        return res
        
        
    def _n_daily_rain_check(self, threshold=0.5):
        """Check the number of rainy days"""
        wdys = self >= threshold
        awd = wdys.groupby(wdys.index.year).sum(min_count=1)
        # ~ awd = wdys.groupby(wdys.index.year).sum()
        mawd = awd.mean()
        
        res = awd.copy()
        res[:] = 0
        assert res.shape == awd.shape
        
        for codeerror, factor in zip([1, MAXERR - 1, MAXERR], [2, 5, 10]):
            #~ print(codeerror, factor)
            #~ print(MissError(codeerror))
            errs = (awd < (mawd / factor)) | (awd > (mawd * factor))
            res[errs] = codeerror
            
        res = res.applymap(MissError)
        return res
        
        
    def _m_daily_rain_check(self, threshold=0.5):
        """Check the intensity of rainy days"""
        wdys = self >= threshold
        wdi = self[wdys]
        awdi = wdi.groupby(wdi.index.year).mean()
        mawdi = wdi.mean()
        
        res = awdi.copy()
        res[:] = 0
        assert res.shape == awdi.shape
        
        for codeerror, factor in zip([1, MAXERR - 1, MAXERR], [2, 5, 10]):
            #~ print(codeerror, factor)
            #~ print(MissError(codeerror))
            errs = (awdi < (mawdi / factor)) | (awdi > (mawdi * factor))
            res[errs] = codeerror
            
        res = res.applymap(MissError)
        return res
        

    def _seasonal_cycle_check(self, monthly_threshold=3):
        """Check the seasonal cycle
        Threshold : mean monthly ranfall n millimeter
        """
        #mean seasonal cycle
        msc = self.groupby(self.index.month)
        msc = msc.mean()
        #months over threshold
        msc = msc >= monthly_threshold
        msc = msc * 1
        #~ print(msc)
        #seasonal cycle
        sc = self.groupby([self.index.year, self.index.month])
        sc = sc.sum()
        sc = sc > 0 #wet month
        sc = sc * 1
        
        years = sorted(set(self.index.year))
        
        dfs = []
        for iyear in years:
            #~ print(iyear)
            sc_diff = sc.loc[iyear] - msc # 0 if both wet/dry month ; -1 if wet month in mean cycle not in year cycle
            sc_notok = sc_diff < 0        #month where rainfall is lacking
            sc_notok = sc_notok.sum()     #number of month where rainfall is lacking
            #~ print(sc_notok)
            #~ print(sc_notok.index.shape)
            #~ print(self.columns.is)
            df_errs = pandas.DataFrame([sc_notok], columns=self.columns, index=[iyear])
            df_errs = df_errs.applymap(MissError)
            #~ print(iyear, df_errs)
            dfs.append(df_errs)
        
        res = pandas.concat(dfs)
        
        return res
        
        
    def mask_annual_values(self, **kwargs):
        """Compute a mask for missing years"""
        matot = self._annual_rainfall_check()
        mndys = self._n_daily_rain_check()
        mmdys = self._m_daily_rain_check()
        msc = self._seasonal_cycle_check()
        
        res = matot + mndys + mmdys + msc
        res = res.applymap(lambda x: x.to_mask_value(**kwargs))
        
        return res
        
        
    def treated_data(self, annual_values=True, min_percent=0.2, **kwargs):
        """
        Return the data with nan for year with erroneous or missings
        """
        if annual_values:
            yrmk = self.mask_annual_values(**kwargs)
        else:
            raise ValueError("Not yet implemented")
            
        assert isinstance(min_percent, Number) and 0 <= min_percent <= 1, f"pb input min percent {min_percent}"
        
        n_sta = self.columns.size      #total number of station
        mnsta = n_sta * min_percent #minnimal number of station
        
        yrsta = yrmk.sum(axis=1) #number of valid stations per year
        rmyrs = yrsta < mnsta      #dataframe year : True if not valid
        rmyrs = rmyrs[rmyrs]       #only non valid years are retained
        rmyrs = rmyrs.index       

        print(f"Years that do not have a sufficient number of stations (nmin={mnsta:.1f}): {rmyrs}")
        #~ print(yrmk)
        yrmk.loc[rmyrs] = False
        #~ print((yrmk == True).sum())
        
        mask = self.fill_yearmask(yrmk)
        
        res = self[mask]
        
        #~ print(res)
        osz = res.index.size
        res = res.dropna(axis=0, how='all')
        nsz = res.index.size
        print(f'nrow with only nan : {osz - nsz}')
        
        mnvl = pandas.Series(res.values.flatten()).dropna().min()
        assert mnvl >= 0, f"daily rain must be > than 0, got {mnvl}"
        return res
        

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--test', default=False, action='store_true')
    parser.add_argument('--ncfile', type=str)
    args = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if args.doctests:
        test_result = doctest.testmod()
        print(test_result)
    
    
    #+++++++++++++++++++++++++++++++#
    #    TEStS                      #
    #+++++++++++++++++++++++++++++++#
    
    if args.test:
        nind = 365*20
        ds = [datetime.datetime(2014, 12, 28) + datetime.timedelta(1*i) for i in range(nind)]
        dt = numpy.random.exponential(scale=15, size=nind*2).reshape(nind, 2)
        drc = DailyRain_Checker(dt, columns=list("ab"), index=ds)

        emm = drc.empty_mask()
        mat = drc._annual_rainfall_check()
        ym = drc.fill_yearmask(mat)
        td = drc.treated_data()
    
    
    #+++++++++++++++++++++++++++++++#
    #    NCFILE                     #
    #+++++++++++++++++++++++++++++++#
    
    if args.ncfile:
        from obelibix.kolinkelly import NcFile

        with NcFile(args.ncfile, 'r', schema='TP') as ncf:
            sta_lons = ncf.get_coordinates('longitude')
            sta_lats = ncf.get_coordinates('latitude')
            sta_nms = ncf.get_metadata('name')
            rains = ncf.get_spatial_time_series()


        rains = DailyRain_Checker(rains.values, columns=sta_nms.values, index=rains.index)


        d = rains.mask_annual_values()

        for icol in d.columns:
            if d[icol][d[icol] == False].size > 0:
                print(icol, "\n", d[icol][d[icol] == False], end="\n\n")

        new_df = rains.treated_data()
