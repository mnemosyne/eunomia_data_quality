"""
For gauge network plot and analysis
"""


##======================================================================================================================##
##                PACKAGE                                                                                               ##
##======================================================================================================================##

import string
import argparse
import doctest


import numpy
import pandas
from matplotlib import pyplot

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_opvl = 1

##======================================================================================================================##
##                RANDOM DF                                                                                             ##
##======================================================================================================================##

def example_op_df(method, nsta=10, nyear=20, seed=0):
    """
    >>> example_op_df("rdm",2,5,0)
              a      b
    1900  False   True
    1901   True  False
    1902   True   True
    1903   True   True
    1904   True   True
    >>> example_op_df("rdm",3,4,1)
              a      b      c
    1900   True   True  False
    1901  False   True   True
    1902   True   True   True
    1903  False  False   True
    >>> example_op_df(0)
              a      b      c      d      e      f      g      h      i      j
    1900  False  False  False  False  False  False  False  False  False  False
    1901  False  False  False  False  False  False  False  False  False  False
    1902  False  False  False  False  False  False  False  False  False  False
    1903  False  False  False  False  False  False  False  False  False  False
    1904  False  False  False  False  False  False  False  False  False  False
    1905  False  False  False  False  False  False  False  False  False  False
    1906  False  False  False  False  False  False  False  False  False  False
    1907  False  False  False  False  False  False  False  False  False  False
    1908  False  False  False  False  False  False  False  False  False  False
    1909  False  False  False  False  False  False  False  False  False  False
    1910  False  False  False  False  False  False  False  False  False  False
    1911  False  False  False  False  False  False  False  False  False  False
    1912  False  False  False  False  False  False  False  False  False  False
    1913  False  False  False  False  False  False  False  False  False  False
    1914  False  False  False  False  False  False  False  False  False  False
    1915  False  False  False  False  False  False  False  False  False  False
    1916  False  False  False  False  False  False  False  False  False  False
    1917  False  False  False  False  False  False  False  False  False  False
    1918  False  False  False  False  False  False  False  False  False  False
    1919  False  False  False  False  False  False  False  False  False  False
    >>> example_op_df(1)
             a     b     c     d     e     f     g     h     i     j
    1900  True  True  True  True  True  True  True  True  True  True
    1901  True  True  True  True  True  True  True  True  True  True
    1902  True  True  True  True  True  True  True  True  True  True
    1903  True  True  True  True  True  True  True  True  True  True
    1904  True  True  True  True  True  True  True  True  True  True
    1905  True  True  True  True  True  True  True  True  True  True
    1906  True  True  True  True  True  True  True  True  True  True
    1907  True  True  True  True  True  True  True  True  True  True
    1908  True  True  True  True  True  True  True  True  True  True
    1909  True  True  True  True  True  True  True  True  True  True
    1910  True  True  True  True  True  True  True  True  True  True
    1911  True  True  True  True  True  True  True  True  True  True
    1912  True  True  True  True  True  True  True  True  True  True
    1913  True  True  True  True  True  True  True  True  True  True
    1914  True  True  True  True  True  True  True  True  True  True
    1915  True  True  True  True  True  True  True  True  True  True
    1916  True  True  True  True  True  True  True  True  True  True
    1917  True  True  True  True  True  True  True  True  True  True
    1918  True  True  True  True  True  True  True  True  True  True
    1919  True  True  True  True  True  True  True  True  True  True
    >>> example_op_df("tri")
             a     b     c      d      e      f      g      h      i      j
    1900  True  True  True   True   True   True   True   True   True   True
    1901  True  True  True   True   True   True   True   True   True   True
    1902  True  True  True   True   True   True   True   True   True   True
    1903  True  True  True   True   True   True   True   True   True   True
    1904  True  True  True   True   True   True   True   True   True   True
    1905  True  True  True   True   True   True   True   True   True  False
    1906  True  True  True   True   True   True   True   True  False  False
    1907  True  True  True   True   True   True   True  False  False  False
    1908  True  True  True   True   True   True  False  False  False  False
    1909  True  True  True   True   True  False  False  False  False  False
    1910  True  True  True   True   True  False  False  False  False  False
    1911  True  True  True   True  False  False  False  False  False  False
    1912  True  True  True   True  False  False  False  False  False  False
    1913  True  True  True   True  False  False  False  False  False  False
    1914  True  True  True   True  False  False  False  False  False  False
    1915  True  True  True  False  False  False  False  False  False  False
    1916  True  True  True  False  False  False  False  False  False  False
    1917  True  True  True  False  False  False  False  False  False  False
    1918  True  True  True  False  False  False  False  False  False  False
    1919  True  True  True  False  False  False  False  False  False  False
    
    """
    ryears = numpy.arange(nyear) + 1900
    rstas = [string.ascii_letters[ista] for ista in range(nsta)]
    df0 = pandas.DataFrame(None, index=ryears, columns=rstas)
    if method == 0:
        vls = numpy.zeros(df0.shape)
    elif method == 1:
        vls = numpy.ones(df0.shape)
    elif method.lower() in ("rdm", "rn", "random"):    
        numpy.random.seed(seed=seed)        
        vls = numpy.random.randint(0, 2, df0.size)
        vls = vls.reshape(df0.shape)
    elif method.lower() in ('tri', "t", "triangle"):
        vls = [[ista * iyr for ista in range(df0.columns.size)] for iyr in range(df0.index.size)]
        vls = numpy.array(vls)
        mnv = vls.mean()
        vls = vls < mnv
    else:
        raise ValueError(f"Dont know which method use: {method}")
     
    res = pandas.DataFrame(vls, index=ryears, columns=rstas)
    res = res.applymap(bool)
    res = NetworkOperation(res)
    return res
    

##======================================================================================================================##
##                NETWORK OP CLASS                                                                                      ##
##======================================================================================================================##

class NetworkOperation(pandas.DataFrame):
    """ Class doc
    
    
    >>> rdf = example_op_df(1)
    >>> rdf.nstation_evolution()
    1900    10
    1901    10
    1902    10
    1903    10
    1904    10
    1905    10
    1906    10
    1907    10
    1908    10
    1909    10
    1910    10
    1911    10
    1912    10
    1913    10
    1914    10
    1915    10
    1916    10
    1917    10
    1918    10
    1919    10
    dtype: int64

    
    >>> rdf.nyear_per_station()
    a    20
    b    20
    c    20
    d    20
    e    20
    f    20
    g    20
    h    20
    i    20
    j    20
    dtype: int64

    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot_nstations_evolution()
    >>> fig.show()

    >>> rdf = rdf.sort_by_nyear()

    
    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot(ms=12)
    >>> fig.show()
    

    >>> a=rdf.add_nyear_per_station_on_axis2()

    >>> ax.grid(1)
    >>> lg=pyplot.legend()

    >>> fig.show()


    >>> fig = pyplot.figure()
    >>> ax = fig.gca()

    >>> a=rdf.plot_nyear_per_station()
    >>> fig.show()
    
    >>> rdf = example_op_df(0)
    >>> rdf.nstation_evolution()
    1900    0
    1901    0
    1902    0
    1903    0
    1904    0
    1905    0
    1906    0
    1907    0
    1908    0
    1909    0
    1910    0
    1911    0
    1912    0
    1913    0
    1914    0
    1915    0
    1916    0
    1917    0
    1918    0
    1919    0
    dtype: int64

    
    >>> rdf.nyear_per_station()
    a    0
    b    0
    c    0
    d    0
    e    0
    f    0
    g    0
    h    0
    i    0
    j    0
    dtype: int64
    
    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot_nstations_evolution()
    >>> fig.show()
    >>> rdf = rdf.sort_by_nyear()

    
    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot(ms=12)
    

    >>> a=rdf.add_nyear_per_station_on_axis2()

    >>> a=ax.grid(1)
    >>> a=pyplot.legend()

    >>> fig.show()
    
    
    >>> rdf = example_op_df("rdm")
    >>> rdf.nstation_evolution()
    1900    8
    1901    3
    1902    6
    1903    6
    1904    7
    1905    6
    1906    3
    1907    5
    1908    7
    1909    5
    1910    4
    1911    3
    1912    9
    1913    3
    1914    7
    1915    5
    1916    4
    1917    4
    1918    3
    1919    3
    dtype: int64

    
    >>> rdf.nyear_per_station()
    a    12
    b    10
    c    10
    d     8
    e    12
    f     9
    g    11
    h    11
    i    11
    j     7
    dtype: int64

    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot_nstations_evolution()
    >>> fig.show()

    >>> rdf = rdf.sort_by_nyear()

    
    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot(ms=12)
    

    >>> a=rdf.add_nyear_per_station_on_axis2()

    >>> a=ax.grid(1)
    >>> a=pyplot.legend()

    >>> fig.show()


    >>> fig = pyplot.figure()
    >>> ax = fig.gca()

    >>> a=rdf.plot_nyear_per_station()
    
    >>> fig.show()
    
    >>> rdf = example_op_df("tri")
    >>> rdf.nstation_evolution()
    1900    10
    1901    10
    1902    10
    1903    10
    1904    10
    1905     9
    1906     8
    1907     7
    1908     6
    1909     5
    1910     5
    1911     4
    1912     4
    1913     4
    1914     4
    1915     3
    1916     3
    1917     3
    1918     3
    1919     3
    dtype: int64

    
    >>> rdf.nyear_per_station()
    a    20
    b    20
    c    20
    d    15
    e    11
    f     9
    g     8
    h     7
    i     6
    j     5
    dtype: int64
    
    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot_nstations_evolution()
    >>> fig.show()
    >>> rdf = rdf.sort_by_nyear()

    
    >>> fig = pyplot.figure()
    >>> ax = fig.gca()
    >>> a=rdf.plot(ms=12)
    

    >>> a=rdf.add_nyear_per_station_on_axis2()

    >>> a=ax.grid(1)
    >>> a=pyplot.legend()

    >>> fig.show()


    >>> fig = pyplot.figure()
    >>> ax = fig.gca()

    >>> a=rdf.plot_nyear_per_station()
    
    >>> fig.show()
    
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """        
        pandas.DataFrame.__init__(self, *args, **kwargs)
        
        vals = self.values.flatten()
        assert all(isinstance(i, (bool, numpy.bool_)) for i in vals), f"Expect boolean got\n{set(type(i) for i in vals)}"
        assert (self.index.map(lambda x: isinstance(x, int) or x.is_integer())).all(), f"years in index, exp int got \n{self.index}"
        assert sorted(set(self.index)) == list(self.index), f"Years pb\n{self.index}"
        assert (self.columns.map(lambda i: isinstance(i, (str, int)))).all(), f"station names in columns, exp str got \n{self.columns}"

    def nstation_evolution(self, percentage=False):
        """Compute the number of station available as a function of time"""
        res = self.sum(axis=1)
        if percentage:
            res = res / self.columns.size
            
        return res
        
    def nyear_per_station(self):
        """Return the number of valid years at each station"""
        res = self.sum(axis=0)
        return res
        
    def sort_by_nyear(self, **kwargs):
        """Return a new object where the stations (columns) are sorted according the number of available years"""
        dicargs = dict(ascending=False)
        dicargs.update(kwargs)
        inds = self.nyear_per_station().sort_values(**dicargs).index
        res = self.loc[:, inds]
        res = NetworkOperation(res)
        return res
        
    def plot_nstations_evolution(self, **kwargs):
        """ Plot the number of station of a function of years"""
        dicargs = dict(marker='o', mec='k', color='0.5', ls=':', mew=2)
        dicargs.update(kwargs)
        
        xy = self.nstation_evolution()
        res = pyplot.plot(xy.index, xy.values, **dicargs)
        return res
        
    def plot_nyear_per_station(self, **kwargs):
        """Plot the number of year at each station"""
        fig = pyplot.gcf()
        ax = fig.gca()
        
        dicargs = dict(marker='o', mec='k', color='0.5', ls=':', mew=2)
        dicargs.update(kwargs)
        
        xy = self.nyear_per_station()
        x = xy.values
        y = numpy.arange(xy.index.size)
        
        res = pyplot.plot(x, y, **dicargs)
        ax.set(yticklabels=xy.index, yticks=y)
        
        return res
        
        
    def plot(self, val_kwargs=None, miss_kwargs=None, **kwargs):
        """Plot valid and missing year in a simple plot"""
        fig = pyplot.gcf()
        ax = fig.gca()
        
        valargs = dict(marker='s', ls='none', mew=2, mec="k", mfc="0.8", ms=10, label="Valid")
        missargs = dict(marker='x', ls='none', mec="k", mfc="w", mew=1, ms=10, label="Missing")
        
        if val_kwargs is not None:
            valargs.update(val_kwargs)
        
        if miss_kwargs is not None:
            missargs.update(miss_kwargs)
        
        valargs.update(kwargs)
        missargs.update(kwargs)
        
        
        
        df = self.transpose()
        #~ years = df.columns
        #~ stations = df.index
        
        yval, ival = numpy.where(df.values)
        ymiss, imiss = numpy.where(df.values == False)
        assert (ival.size + imiss.size) == ymiss.size + yval.size == df.size
        
        assert all(df.iloc[j, i] == True for i, j in zip(ival, yval)), "pb code"
        assert all(df.iloc[j, i] == False for i, j in zip(imiss, ymiss)), "pb code"
        
        xval = df.columns[ival]
        xmiss = df.columns[imiss]
        
        lineval = pyplot.plot(xval, yval, **valargs)
        linemiss = pyplot.plot(xmiss, ymiss, **missargs)
        
        dxy = 0.5
        xtcks = df.columns
        ytcks = numpy.arange(df.index.size)
        ytcklbs = df.index
        #~ xtcklbs = df.columns
        
        ylim = (ytcks.min() - dxy, ytcks.max() + dxy)
        xlim = (xtcks.min() - dxy, xtcks.max() + dxy)
        
        ax.set(yticks=ytcks, yticklabels=ytcklbs, ylim=ylim, xlim=xlim) #, xticks=xtcks
        
        res = dict(val=lineval, miss=linemiss)
        
        return res
        
    def add_nyear_per_station_on_axis2(self):
        """Add the number of year per station on a secondary axis. Automatically get the yticks, yticklabels"""
        fig = pyplot.gcf()
        ax = fig.gca()
        
        ytcks = ax.get_yticks()
        xtcks = ax.get_xticks()
        ytklbs = ax.get_yticklabels()
        ylim = ax.get_ylim()
        xlim = ax.get_xlim()
        stations = [i.get_text() for i in ytklbs]
        assert len(stations) == self.columns.size, "uncohrent size"
        assert sorted(stations) == sorted(self.columns), "uncohrent stations"

        yr_sm = self.nyear_per_station()
        yr_sm = yr_sm.loc[stations]
        
        res = ax.twinx()
        res.set(yticks=ytcks, yticklabels=yr_sm, ylim=ylim, xlim=xlim, xticks=xtcks)

        return res





##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--example', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.example:
        rdf = example_op_df('tri')
        print(rdf)

        rdf = NetworkOperation(rdf)

        print(rdf.nstation_evolution())

        f = pyplot.figure()
        a = f.gca()
        rdf.plot_nstations_evolution()

        pyplot.grid(1)
        f.show()

        rdf = rdf.sort_by_nyear()

        
        f = pyplot.figure()
        a = f.gca()
        rdf.plot(ms=12)
        

        rdf.add_nyear_per_station_on_axis2()

        a.grid(1)
        pyplot.legend()

        f.show()


        f = pyplot.figure()


        rdf.plot_nyear_per_station()
        
        f.show()
